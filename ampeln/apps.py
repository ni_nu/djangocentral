from django.apps import AppConfig


class AmpelnConfig(AppConfig):
    name = 'ampeln'
