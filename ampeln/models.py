from django.db import models
from django.utils import timezone

# Create your models here.


class Collections(models.Model):
    name = models.CharField(max_length=50)
    functions = models.IntegerField()

    def __str__(self):
        return self.name


class Devices(models.Model):
    mac = models.CharField(max_length=200)
    active = models.BooleanField(default=False)
    Collection = models.ForeignKey(Collections, on_delete=models.SET_NULL, null=True)

    def create(self):
        self.save()

    def __str__(self):
        return self.mac
